<?php
    class Htmalpage{
        protected $title = "ADI REGEV, ID: 312420268";
        protected $body = "aregev238@gmail.com";
        public function view(){
            echo "<html>
            <head>
              <title>$this->title</title>
            </head>
            <body>
            </body>
            </html>
            ";
        }
        function __construct($title = "" , $body = ""){
            if($title != ""){
                $this->title = $title;
            }
            
            if($body != ""){
                $this->body = $body;
            }
        }
    }
    class coloredMessage extends Htmalpage {
        protected $color;
        public function __set($property,$value){
            if ($property == 'color'){
                $colors = array('red','blue','green','orange');
                if(in_array($value,$colors)){
                    $this->color = $value; 
                } else{
                    $this->body = "Invalid color, please choose a different color!";
                }
            }
        }
        public function view(){
            echo "
            <html>
            <head>
                <title>$this->title</title>
            </head>
            <body>
                <p style = 'color:$this->color'  >$this->body</p>
            </body>
            </html>
            ";
        }
    }

    class sizedMessage extends coloredMessage {
        protected $size;
        public function __set($property,$value){
            if ($property == 'size'){
                $sized=range(10,24);
                if(in_array($value,$sized)){
                    $this->size = $value; 
                }
                else{
                    $this->body = "Invalid size, please choose a different size!";
                }
            }
            elseif ($property == 'color') {
                parent::__set($property,$value);
            }
        }
        public function view(){
            echo "
            <html>
            <head>
                <title>$this->title</title>
            </head>
            <body> 
                <p style = 'color:$this->color; font-size:$this->size;' >$this->body</p>
            </body>
            </html>
            ";
        }
    }
?>
